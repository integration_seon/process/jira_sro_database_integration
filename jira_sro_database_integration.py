from pprint import pprint
import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
import logging
import requests

logging.basicConfig(level=logging.INFO)

args = {
    'owner': 'seon',
    'start_date': airflow.utils.dates.days_ago(1)
}

dag = DAG (
    
    dag_id= 'jira_sro_database_integration',
    default_args=args,
    schedule_interval = None,
)

def retrive_user(**kwargs):
    pprint ("retrive tasks")
    r = requests.post("http://localhost:5000/user/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_scrum_project(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/scrumproject/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_scrum_project_team(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/scrumprojectteam/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_scrum_development_team(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/scrumdevelopmentteam/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_team_member(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/teammember/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_sprint(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/sprint/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_scrum_development_task(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/scrumdevelopmenttask/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_epic(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/epic/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_product_backlog(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/productbacklog/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_sprint_backlog(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/sprintbacklog/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

def retrive_user_story(**kwargs):
    pprint("retrive scrum project")
    r = requests.post("http://localhost:5000/userstorygit/70a94b97-de05-44d5-b294-6b03740e80e0")
    pprint(r.url)

extract_user = PythonOperator(
    task_id = 'extract_user',
    provide_context=True,
    python_callable=retrive_user, 
    dag=dag,
)

extract_scrum_project = PythonOperator(
    task_id = 'extract_scrum_project',
    provide_context=True,
    python_callable=retrive_scrum_project, 
    dag=dag,
)

extract_scrum_project_team = PythonOperator(
    task_id = 'extract_scrum_project_team',
    provide_context=True,
    python_callable=retrive_scrum_project_team, 
    dag=dag,
)

extract_scrum_development_team = PythonOperator(
    task_id = 'extract_scrum_development_team',
    provide_context=True,
    python_callable=retrive_scrum_development_team, 
    dag=dag,
)
extract_team_member = PythonOperator(
    task_id = 'extract_team_member',
    provide_context=True,
    python_callable=retrive_team_member, 
    dag=dag,
)
extract_epic = PythonOperator(
    task_id = 'extract_epic',
    provide_context=True,
    python_callable=retrive_epic, 
    dag=dag,
)
extract_user_story = PythonOperator(
    task_id = 'extract_user_story',
    provide_context=True,
    python_callable=retrive_user_story, 
    dag=dag,
)
extract_scrum_development_task = PythonOperator(
    task_id = 'extract_scrum_development_task',
    provide_context=True,
    python_callable=retrive_scrum_development_task, 
    dag=dag,
)
extract_sprint = PythonOperator(
    task_id = 'extract_sprint',
    provide_context=True,
    python_callable=retrive_sprint, 
    dag=dag,
)

## Workflow
extract_scrum_project >> extract_user >> extract_scrum_project_team >> extract_scrum_development_team >> extract_team_member >> extract_epic >> extract_user_story >> extract_scrum_development_task >> extract_sprint